#!/bin/bash

set -x

sed -e 's/slash\}libtool/slash\}${HOST_SYS}-libtool/' -i acinclude.m4
sed -e 's|r/libtool|r/${HOST_SYS}-libtool|' -i runtime/mono-wrapper.in
sed -e 's|r/libtool|r/${HOST_SYS}-libtool|' -i runtime/monodis-wrapper.in

