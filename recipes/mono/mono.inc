SUMMARY = "An Open Source implementation of the Microsoft's .NET Framework"
DESCRIPTION = "This is part of the Mono project - http://mono-project.com"
HOMEPAGE = "http://mono-project.com"
BUGTRACKER = "http://bugzilla.xamarin.com/"
LICENSE = "GPL-2.0"

RECIPE_TYPES = "native machine"

DEPENDS = "libm libpthread librt libdl"
DEPENDS:>machine += " native:mono"

inherit autotools

SRC_URI = "http://download.mono-project.com/sources/mono/mono-${PV}.tar.bz2 \
	file://dllmap-config.in.diff \
    file://patch-libtool.sh \
    file://fix-mono-2-pc.diff \
"

EXTRA_OECONF += "    --with-sigaltstack=no --with-mcs-docs=no mono_cv_uscore=no"


#
# Mono is compiled in two steps when cross-compiling:
# 1. build host mono compiler and compile class libraries on host (since
#    they're platform independent)
# 2. build target system mono runtime and compiler
#
# Therefore, compile the class libs after compiling (this will build them
# twice in in the native version of the recipe, but required for machine
# version)
#
# Then, when everything has been installed, copy the class libraries from the
# native mono build (where they can be compiled).
#
do_compile[postfuncs] += "do_compile_class_libs"
do_compile_class_libs() {
	echo "ok"
    # XXX: foo/bar/mono?
    make EXTERNAL_MCS="${S}/mcs/class/lib/monolite/basic.exe" EXTERNAL_RUNTIME="${S}/foo/bar/mono"
}

DO_INSTALL_POSTFUNCS = ""
DO_INSTALL_POSTFUNCS:machine = "do_install_class_libs"
do_install[postfuncs] += "${DO_INSTALL_POSTFUNCS}"
do_install_class_libs () {
	cp -aR ${STAGE_DIR}/native/lib/${PN}/ ${D}${libdir}/${PN}/
	cp -af ${STAGE_DIR}/native/etc/${PN} ${D}${sysconfdir}

    # Removing *.a and *.la since I can't figure out how 
    #find ${D}${libdir} -name *.dll.so -o -name *.exe.so -o -name *.a -o -name *.la | xargs -i rm {} 
}

RDEPENDS_${PN} += "libm libpthread librt libdl"

PACKAGES += "${PN}-source-libs"

FILES_${PN}-doc += "${datadir}/libgc-mono"
FILES_${PN} += "${datadir}/mono-2.0"
FILES_${PN} += "${sysconfdir}/mono/*"
FILES_${PN} += "${libdir}/libmono*so*"
FILES_${PN} += "${libdir}/libMono*so*"

FILES_${PN}-source-libs += "${libdir}/mono-source-libs/*.cs"

